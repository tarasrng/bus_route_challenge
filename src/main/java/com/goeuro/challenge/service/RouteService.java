package com.goeuro.challenge.service;

import com.goeuro.challenge.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service handling route data.
 */
@Service
public class RouteService {

    @Autowired
    RouteRepository routeRepository;

    /**
     * Checks if specified stations are connected with a direct route.
     * @param departureId departure station id
     * @param arrivalId arrival station id
     * @return route DTO
     */
    public boolean checkDirectRoute(int departureId, int arrivalId) {
        return routeRepository.checkDirectRoute(departureId, arrivalId);
    }
}
