package com.goeuro.challenge.controller;

import com.goeuro.challenge.dto.Route;
import com.goeuro.challenge.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller that handles requests related to routes.
 */
@RestController
@RequestMapping("api")
public class RouteController {
    @Autowired
    RouteService routeService;

    /**
     * Checks if specified stations are connected with a direct route.
     * @param departureId departure station id
     * @param arrivalId arrival station id
     * @return route DTO
     */
    @RequestMapping(value = "direct", method = RequestMethod.GET)
    public Route checkDirectRoute(@RequestParam("dep_sid") int departureId,
                                  @RequestParam("arr_sid") int arrivalId) {
        boolean isRouteDirect = routeService.checkDirectRoute(departureId, arrivalId);
        return new Route(departureId, arrivalId, isRouteDirect);
    }
}
