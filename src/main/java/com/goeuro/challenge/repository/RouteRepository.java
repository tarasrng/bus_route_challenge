package com.goeuro.challenge.repository;

import org.springframework.stereotype.Repository;

/**
 * Provides route data.
 */
@Repository
public interface RouteRepository {
    /**
     * Checks if specified stations are connected with a direct route.
     * @param departureId departure station id
     * @param arrivalId arrival station id
     * @return route DTO
     */
    boolean checkDirectRoute(int departureId, int arrivalId);
}
