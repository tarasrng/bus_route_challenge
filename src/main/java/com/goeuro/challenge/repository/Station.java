package com.goeuro.challenge.repository;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Station {
    private int id;
    private int position;
}
