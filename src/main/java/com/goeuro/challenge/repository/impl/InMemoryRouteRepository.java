package com.goeuro.challenge.repository.impl;

import com.goeuro.challenge.repository.RouteRepository;
import com.goeuro.challenge.repository.Station;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Simple in-memory data storage implementation.
 *
 * <p>Provides a quick startup/request processing time.</p>
 *
 * <p>In memory storage allows quick reads,
 * but this could lead to big memory consumption (~ 350mb in worst case scenario)
 * when input file contains maximum amount of routes/stations.</p>
 *
 * <p>Provides a quick startup/request processing time.
 * Sorts input data during the load, and uses binary search during route checking.</p>
 *
 */
@Slf4j
@Repository
public class InMemoryRouteRepository implements RouteRepository {

    @Value("${input.path}")
    private String filePath;

    private static List<List<Station>> storage;
    private static Comparator<Station> stationsComparator = Comparator.comparingInt(Station::getId);

    @Override
    public boolean checkDirectRoute(int departureId, int arrivalId) {
        return storage.parallelStream()
                .anyMatch(stations -> {
                    Station departureIdArray = new Station(departureId, -1);
                    Station arrivalIdIdArray = new Station(arrivalId, -1);
                    int firstStationIdx = Collections.binarySearch(stations, departureIdArray,
                            stationsComparator);
                    int secondStationIdx = Collections.binarySearch(stations, arrivalIdIdArray,
                            stationsComparator);
                    boolean routeContainsStations = firstStationIdx > -1 && secondStationIdx > -1;
                    return routeContainsStations && stations.get(firstStationIdx).getPosition()
                            <= stations.get(secondStationIdx).getPosition();
                });
    }

    @PostConstruct
    private void init() {
        log.info("Starting data initialization from " + filePath
                + ". It may take few minutes if the file is huge.");
        try (Stream<String> busRoutesStream = Files.lines(Paths.get(filePath));
             Stream<String> routeLinesStream = Files.lines(Paths.get(filePath))) {
            int busRoutes = Integer.parseInt(busRoutesStream.findFirst().orElse("0"));
            storage = new ArrayList<>(busRoutes);
            routeLinesStream.skip(1).forEach(routeLine -> {
                List<Station> stations = new ArrayList<>();
                String[] stationsStringArr = routeLine.split(" ");
                IntStream.range(1, stationsStringArr.length).forEach((position) ->
                        stations.add(new Station(Integer.parseInt(stationsStringArr[position]), position)));
                stations.sort(stationsComparator);
                storage.add(stations);
            });
        } catch (IOException e) {
            log.error("Error during data initialization. ", e);
        }
    }

}
