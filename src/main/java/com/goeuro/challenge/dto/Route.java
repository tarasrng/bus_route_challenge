package com.goeuro.challenge.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Route {
    private int dep_sid;
    private int arr_sid;
    private boolean direct_bus_route;
}
