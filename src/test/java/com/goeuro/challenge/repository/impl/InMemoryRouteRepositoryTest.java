package com.goeuro.challenge.repository.impl;

import com.goeuro.challenge.repository.RouteRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"input.path=data/example.txt"})
public class InMemoryRouteRepositoryTest {

    @TestConfiguration
    static class TestConfig {
        @Bean
        public RouteRepository restTemplateBuilder() {
            return new InMemoryRouteRepository();
        }
    }

    @Autowired
    private RouteRepository routeRepository;

    @Test
    public void checkDirectRoute() {
        assertTrue(routeRepository.checkDirectRoute(153, 24));
        assertTrue(routeRepository.checkDirectRoute(150, 140));
        assertTrue(routeRepository.checkDirectRoute(106, 11));
        assertTrue(routeRepository.checkDirectRoute(121, 155));
        assertTrue(routeRepository.checkDirectRoute(12, 174));
        assertTrue(routeRepository.checkDirectRoute(12, 12));

        assertFalse(routeRepository.checkDirectRoute(10, 1));
        assertFalse(routeRepository.checkDirectRoute(10, 106));
        assertFalse(routeRepository.checkDirectRoute(100500, 106));
        assertFalse(routeRepository.checkDirectRoute(106, 100500));
        assertFalse(routeRepository.checkDirectRoute(5, 150));
        assertFalse(routeRepository.checkDirectRoute(17, 142));
        assertFalse(routeRepository.checkDirectRoute(1, 153));
        assertFalse(routeRepository.checkDirectRoute(13, 153));
        assertFalse(routeRepository.checkDirectRoute(12, 148));

    }
}
