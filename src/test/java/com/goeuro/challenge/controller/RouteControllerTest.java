package com.goeuro.challenge.controller;

import com.goeuro.challenge.service.RouteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RouteController.class)
public class RouteControllerTest {

    private static final String BASE_URL = "/api/";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RouteService routeService;


    @Test
    public void checkExistingConnection() throws Exception {
        given(this.routeService.checkDirectRoute(1, 2))
                .willReturn(true);
        invokeDirect(1, 2)
                .andExpect(status().isOk())
                .andExpect(content().json("{'dep_sid':1, 'arr_sid':2, 'direct_bus_route':true}"));
    }

    @Test
    public void checkMissingConnection() throws Exception {
        given(this.routeService.checkDirectRoute(2, 1))
                .willReturn(false);
        invokeDirect(2, 1)
                .andExpect(status().isOk())
                .andExpect(content().json("{'dep_sid':2, 'arr_sid':1, 'direct_bus_route':false}"));
    }

    private ResultActions invokeDirect(int departureId, int arrivalId) throws Exception {
        return mvc.perform(
                get(String.format("%sdirect?dep_sid=%d&arr_sid=%d", BASE_URL, departureId, arrivalId))
                        .accept(MediaType.APPLICATION_JSON));
    }

}